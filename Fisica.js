class Fisica{

    constructor(){
        this.a = 0;
    }

    resultados(){

        let g = 9.8;
        let Vo= document.getElementById('Vo').value;
        let Wo= (document.getElementById('Wo').value * Math.PI) / 180;

        let Vx= (Vo* Math.cos(Wo));
        let Vy= (Vo* Math.sin(Wo));
        let T= (2* Vo * Math.sin(Wo)/g);
        let X= (Vo * Math.cos(Wo)* T);
        let Ty= (0-Vy)/-g
        let Y= (((Vo*Math.sin(Wo))*(Vo*Math.sin(Wo)))/(2*g));
        let XTy = (Vo * Math.cos(Wo)* Ty)

        document.getElementById('Dm').innerHTML= X;
        document.getElementById('DAm').innerHTML= XTy;
        document.getElementById('Am').innerHTML= Y;
        document.getElementById('Tam').innerHTML= Ty;
        document.getElementById('Tt').innerHTML= T;

        this.X=X;
        this.XTy=XTy;
        this.Y=Y;
        this.Ty=Ty;
        this.T=T; 
    
        if (this.X<600) {
        this.a = 1;
        return this.a;
        }else{
            if (this.X>=600 && this.X<1000) {
                this.a = 2;
                return this.a;
                }else{
                    if (this.X>=1000) {
                        this.a = 3;
                        return this.a;
                        }   
                }
        }
    }

    reiniciar(){

        let a = document.getElementById('Dm');
        let b = document.getElementById('DAm');
        let c = document.getElementById('Am');
        let e = document.getElementById('Tam');
        let f = document.getElementById('Tt');

        a.innerHTML=" ##";
        b.innerHTML=" ##";
        c.innerHTML=" ##";
        d.innerHTML=" ##";
        e.innerHTML=" ##";
        f.innerHTML=" ##";

        let l = document.getElementById('d');
        l.innerHTML= "<canvas width='1000' height='400' id='Draw' style='position:absolute; top:100px; left:475px;' ></canvas>"
        
        this.plano();  
    }
    plano(){

        let Lienzo = document.getElementById('Draw');
        let ctx = Lienzo.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(30,375);
        ctx.lineTo(30,15);
        ctx.stroke();
        ctx.closePath();

        ctx.beginPath();
        ctx.moveTo(30,375);
        ctx.lineTo(950,375);
        ctx.stroke();
        ctx.closePath();

        ctx.beginPath();
        ctx.fillText("X",953,375,30);
        ctx.closePath();
        ctx.fill();

        ctx.beginPath();
        ctx.fillText("Y",30,13,30);
        ctx.closePath();
        ctx.fill();
    }

    dibujos(){
        
        this.reiniciar();
        this.resultados();

        let A= document.getElementById('Wo').value;
        let Lienzo = document.getElementById('Draw');
        let ctx = Lienzo.getContext("2d");
        
        ctx.save();

        ctx.beginPath();
        ctx.arc(35,380,13,0,Math.PI*2,true);  
        ctx.fill();
        ctx.closePath();
        
        ctx.beginPath();
        ctx.translate(40,377);
        ctx.rotate(- A * Math.PI / 180 );
        ctx.translate(-40,-377);
        ctx.fillRect(30, 359, 40, 15);
        ctx.fill();
        ctx.closePath();
        ctx.restore();
        
//--------------------------------------------

    let xf = this.X  + 40;
    let yf = 0;
    let x = (xf / 2) ;
    let y = -this.Y*2;
    switch (this.a) {
        case this.a=1:

            ctx.beginPath(); 
            ctx.translate(30,375);
            ctx.strokeStyle="000000"; 
            ctx.lineWidth=3; 
            ctx.moveTo(0,0);
            ctx.quadraticCurveTo(x, y, xf, yf); 
            ctx.stroke(); 
            ctx.closePath()
            ctx.restore();
    break;

    case this.a=2:

            ctx.beginPath(); 
            ctx.translate(30,375);
            ctx.strokeStyle="000000"; 
            ctx.lineWidth=3; 
            ctx.moveTo(0,0);
            ctx.quadraticCurveTo(x/2, y/2, xf/2, yf/2); 
            ctx.stroke(); 
            ctx.closePath()
            ctx.restore();
    break;
    case this.a=3:

            ctx.beginPath(); 
            ctx.translate(30,375);
            ctx.strokeStyle="000000"; 
            ctx.lineWidth=3; 
            ctx.moveTo(0,0);
            ctx.quadraticCurveTo(x/4, y/4, xf/4, yf/4); 
            ctx.stroke(); 
            ctx.closePath()
            ctx.restore();
    break;
        default:
            break;
    }
//--------------------------------
    }
    
}